'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
 */
const menuBtn = document.getElementById('menuBtn');
const menu = document.getElementById('menu');

menuBtn.addEventListener('click', () => menu.classList.toggle('menu-opened'));
menu.addEventListener('click', () => menu.classList.remove('menu-opened'));

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 */
const field = document.getElementById('field');
const movedBlock = document.getElementById('movedBlock');

field.addEventListener('click', (e) => {
  const { x, y } = field.getBoundingClientRect();
  const { width, height } = movedBlock.getBoundingClientRect();

  movedBlock.style.left = `${e.clientX - x - width/2}px`;
  movedBlock.style.top = `${e.clientY - y - height/2}px`;
});

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 */
const messager = document.getElementById('messager');

messager.addEventListener('click', ({ target }) => {
  if (target.classList.contains('remove')) {
    target.closest('div.message').style.display = 'none';
  }
});

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
 */
const links = document.getElementById('links');

links.addEventListener('click', (e) => {
  if (e.target.tagName === 'A') {
    const answer = confirm('Точно перейти по указанной ссылке?');
    if (!answer) {
      e.preventDefault();
    }
  }
});

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
 */
const taskHeader = document.getElementById('taskHeader');
const fieldHeader = document.getElementById('fieldHeader');

fieldHeader.addEventListener('input', () => {
  const value = fieldHeader.value.trim();
  if (value) {
    taskHeader.textContent = value;
  }
});